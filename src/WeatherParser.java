import java.io.*;
import java.util.*;

public class WeatherParser {
    public static void main(String[] args) throws IOException {
        WeatherParser parses = new WeatherParser();
        File file = new File("C:\\Users\\Ildar\\OneDrive\\Рабочий стол\\dataexport_20210320T064822.csv");
        parses.getInfoAboutWeather(file);
    }
    public void getInfoAboutWeather(File file) throws IOException {
        List<WeatherInfo> data = makeFileWithData(file);
    double medianTemperature = 0;
    double medianHumidity = 0;
    double medianWindSpeed = 0;
    double minimalHumidity = Double.MAX_VALUE;
    double maxWind = Double.MIN_VALUE;
    double maxTemp = Double.MIN_VALUE;
    String dayMaxTemp = "";
    int hourMaxTemp = -1;
    int minimalHumidityHour = -1;
    String minimalHumidityDay = "";
    String maxWindData = "";
    int maxWindHour = -1;
        Map<String , Integer > mostCommonDirection = new HashMap();
        fillMap(mostCommonDirection);
    for (WeatherInfo weatherInfo : data) {
        double[] medianTempAndHourMaxAndMaxTemp = weatherInfo.getMedianDailyTemperatureAndHourMaxAndMaxTemp();
        medianTemperature += medianTempAndHourMaxAndMaxTemp[0];
        if (maxTemp < medianTempAndHourMaxAndMaxTemp[2] ) {
            dayMaxTemp = weatherInfo.data;
            hourMaxTemp = (int) medianTempAndHourMaxAndMaxTemp[1];
            maxTemp = medianTempAndHourMaxAndMaxTemp[2];
        }

        double[] medianHumidityAndMin = weatherInfo.getMedianDailyHumidityAndMinimalHumidityAndHour();
        medianHumidity += medianHumidityAndMin[0];
        if (minimalHumidity > medianHumidityAndMin[1]) {
            minimalHumidity = medianHumidityAndMin[1];
            minimalHumidityHour = (int) medianHumidityAndMin[2];
            minimalHumidityDay = weatherInfo.data;
        }
        double[] medianDailyWindSpeedAndMax = weatherInfo.getMedianDailyWindSpeedAndMaxAndHour();
        medianWindSpeed += medianDailyWindSpeedAndMax[0];
        if (maxWind < medianDailyWindSpeedAndMax[1]) {
            maxWind = medianDailyWindSpeedAndMax[1];
            maxWindData = weatherInfo.data;
            maxWindHour = (int) medianDailyWindSpeedAndMax[2];
        }
        weatherInfo.shareWindDirection(mostCommonDirection);
    }
        File generatedData = new File("src\\WindAnswer.txt");
        generatedData.createNewFile();
        FileWriter fileWriter = new FileWriter(generatedData);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write("Среднняя температура: " + medianTemperature/data.size() + "\n" +
                "Средняя влажность: " + medianHumidity/data.size() + "\n" +
                "Средняя скорость ветра: " + medianWindSpeed/data.size() + "\n" +
                "Макс температура " + maxTemp + " была в " + dayMaxTemp + " в " + hourMaxTemp + "\n" +
                "Самая низкая влажность: " + minimalHumidity + " была " + minimalHumidityDay + " в " + minimalHumidityHour + "\n" +
                "Самый сильный ветер: " + maxWind + " был " + maxWindData + " в " + maxWindHour + "\n" +
                "Самое популярное направление: " + getMostCommonDirFromMap(mostCommonDirection)
        );
        bufferedWriter.close();
        System.out.println("File generated, " +
                "path: src\\WindAnswer.txt");
    }

    public String getMostCommonDirFromMap(Map<String, Integer> map) {
        int max  = Integer.MIN_VALUE;
        Collection<Integer> values = map.values();
        for (Integer integer : values) {
            if (integer > max) {
                max = integer;
            }
        }
        Set<String> set = map.keySet();
        for (String str: set) {
            if (map.get(str) == max) {
                return str;
            }
        }
        return null;
    }


    public void fillMap(Map<String, Integer> mostCommonDirection) {
        mostCommonDirection.put("N", 0);
        mostCommonDirection.put("NE", 0);
        mostCommonDirection.put("E", 0);
        mostCommonDirection.put("SE", 0);
        mostCommonDirection.put("S", 0);
        mostCommonDirection.put("SW", 0);
        mostCommonDirection.put("W", 0);
        mostCommonDirection.put("NW", 0);
    }

    public List<WeatherInfo> makeFileWithData(File file) throws IOException {
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String readData = bufferedReader.readLine();
        boolean isFind = false;
        while (!isFind) {
            if (!readData.contains("timestamp")) {
                readData = bufferedReader.readLine();
            } else {
                isFind = true;
            }
        }
        List<WeatherInfo> weatherInfoArrayList = new ArrayList<>();
        isFind = false;
         readData = bufferedReader.readLine();
        while (readData!= null) {
            WeatherInfo weatherInfo = new WeatherInfo();
            setTime(weatherInfo, readData); //Поставили дату
            readHourInfo(readData, weatherInfo);
            readDailyWeather(bufferedReader, weatherInfo);//заполнили информацию по часам
            weatherInfoArrayList.add(weatherInfo);
            readData = bufferedReader.readLine();
        }
        return weatherInfoArrayList;

    }
    private void setTime (WeatherInfo weather, String str) {
        String[] weatherInfo = str.split(",");
        String[] dataAndTime = weatherInfo[0].split("T");
        weather.data = dataAndTime[0];
    }

    private void readDailyWeather(BufferedReader reader, WeatherInfo weatherInfo) throws IOException {
        for (int i = 0; i < 23; i ++) {
            String str = reader.readLine();
            readHourInfo(str, weatherInfo);
        }
    }
    private void readHourInfo(String str, WeatherInfo weatherDaily) {
        String[] weatherInfo = str.split(",");
        String[] dataAndTime = weatherInfo[0].split("T");
     //на вход принимает строку (погода за час и записывает в файл)
        int time = Integer.parseInt(dataAndTime[1]);
        time = time/100;
        weatherDaily.temperature[time] = Double.parseDouble(weatherInfo[1]);
        weatherDaily.relativeHumidity[time] = Double.parseDouble(weatherInfo[2]);
        weatherDaily.windSpeed[time] = Double.parseDouble(weatherInfo[3]);
        weatherDaily.windDirection[time] = Double.parseDouble(weatherInfo[4]);
    }
}
