import java.util.Map;

public class WeatherInfo {
    String data;
    double[] temperature = new double[24];
    double[] relativeHumidity = new double[24];
    double[] windSpeed = new double[24];
    double[] windDirection = new double[24];


    public double[] getMedianDailyTemperatureAndHourMaxAndMaxTemp() {
        double sumDailyTemperature = 0;
        int hour = 0;
        int i = 0;
        double maxTemp = Double.MIN_VALUE;
        for (double digit : this.temperature) {
            sumDailyTemperature += digit;
            if (digit > maxTemp) {
                maxTemp = digit;
                hour = i;
            }
            i++;
        }
        return new double[] {sumDailyTemperature/24, hour, maxTemp};
    }
    public double[] getMedianDailyHumidityAndMinimalHumidityAndHour() {
        double humiditySum = 0;
        double hour = 0;
        int i = 0;
        double minimalHumidity = Double.MAX_VALUE;
        for (double digit : this.relativeHumidity) {
            humiditySum += digit;
            if (minimalHumidity > digit) {
                minimalHumidity = digit;
                hour = i;
            }
            i++;
        }
        double[] result = {humiditySum / 24, minimalHumidity, hour};
        return result;
    }
    public double[] getMedianDailyWindSpeedAndMaxAndHour() {
        double sumOfWindSpeed = 0;
        double hour = 0;
        int i = 0;
        double maxWind = Double.MIN_VALUE;
        for (double digit : this.windSpeed) {
            sumOfWindSpeed += digit;
            if (maxWind < digit) {
                maxWind = digit;
                hour = i;
            }
            i++;
        }
        double[] result = {sumOfWindSpeed / 24, maxWind, hour};
        return result;
    }

    public void shareWindDirection(Map <String, Integer> map) {
        for (double direction : this.windDirection) {
            String dir = mostCloseDirection(direction);
            map.put(dir, map.get(dir) + 1);
        }
    }
    public String mostCloseDirection(double direction) {
        String result;
        double minimalDistance = direction; // как будто расстояние от N
        result = "N";
        if (Math.abs(45 - direction) < minimalDistance) {
            result = "NE";
            minimalDistance = Math.abs(45 - direction);
        }
        if (Math.abs(90 - direction) < minimalDistance) {
            result = "E";
            minimalDistance = Math.abs(90 - direction);
        }
        if (Math.abs(135 - direction) < minimalDistance) {
            result = "SE";
            minimalDistance = Math.abs(135 - direction);
        }
        if (Math.abs(180 - direction) < minimalDistance) {
            result = "S";
            minimalDistance = Math.abs(180 - direction);
        }
        if (Math.abs(225 - direction) < minimalDistance) {
            result = "SW";
            minimalDistance = Math.abs(225 - direction);
        }
        if (Math.abs(270 - direction) < minimalDistance) {
            result = "W";
            minimalDistance = Math.abs(270 - direction);
        }
        if (Math.abs(315 - direction) < minimalDistance) {
            result = "NW";
            minimalDistance = Math.abs(315 - direction);
        }
        if (Math.abs(360 - direction) < minimalDistance) {
            result = "N";
        }
        return  result;
    }

}
